import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:dio_http2_adapter/dio_http2_adapter.dart';
import 'package:flutterapp/api/api.dart';
import 'package:flutterapp/api/entity_factory.dart';

class DioManager {
  static final DioManager _shared = DioManager._internal();

  factory DioManager() => _shared;
  Dio dio;

  DioManager._internal() {
    if (dio == null) {
      BaseOptions options = BaseOptions(
        baseUrl: Api.BASE_API,
        contentType: Headers.jsonContentType,
        responseType: ResponseType.json,
        receiveDataWhenStatusError: false,
        connectTimeout: 30000,
        receiveTimeout: 15000,
//          headers: {
//            "Response headers": "*",
//            "Access-Control-Allow-Headers": "true"
//          }
      );
      dio = Dio()
            ..options = options
            ..interceptors.add(LogInterceptor())
//        ..httpClientAdapter = Http2Adapter(ConnectionManager(
//          idleTimeout: 10000,
//          // Ignore bad certificate
//          onClientCreate: (_, config) => config.onBadCertificate = (_) => true,
//        ))
          ;
    }
  }

  // 请求，返回参数为 T
  // method：请求方法，NWMethod.POST等
  // path：请求地址
  // params：请求参数
  // success：请求成功回调
  // error：请求失败回调
  Future request<T>(NWMethod method, String path,
      {Map params, Function(T) success, Function(ErrorEntity) error}) async {
    try {
      if (params == null) {
        params = new Map();
      }
      params['projectCode'] = "MSYX";
      params['projectId'] = "1";

      Map<String, dynamic> headers = new Map();
//      headers['Response headers'] = "*";
//      headers['Access-Control-Allow-Headers'] = "true";

      Response response = await dio.request(path,
          data: params,
          options: Options(method: NWMethodValues[method], headers: headers));
      if (response != null) {
        print("success response = $response");
        BaseEntity entity = BaseEntity<T>.fromJson(response.data);
        if (entity.status == 200) {
          success(entity.data);
        } else {
          error(ErrorEntity(status: entity.status, message: entity.message));
        }
      } else {
        error(ErrorEntity(status: -1, message: "未知错误"));
      }
    } on DioError catch (e) {
      error(createErrorEntity(e));
    }
  }

  Future requestOld<T>(NWMethod method, String path,
      {Map params, Function(T) success, Function(ErrorEntity) error}) async {
    try {
      if (params == null) {
        params = new Map();
      }
      params['projectCode'] = "MSYX";
      params['projectId'] = "1";

      Map<String, dynamic> headers = new Map();
//      headers['Response headers'] = "*";
//      headers['Access-Control-Allow-Headers'] = "true";

      Response response = await dio.request(path,
          data: params,
          options: Options(method: NWMethodValues[method], headers: headers));
      if (response != null) {
        print("success response = $response");
        OldBaseEntity entity = OldBaseEntity<T>.fromJson(response.data);
        if (entity.status == 200) {
          success(entity.result);
        } else {
          error(ErrorEntity(status: entity.status, message: entity.message));
        }
      } else {
        error(ErrorEntity(status: -1, message: "未知错误"));
      }
    } on DioError catch (e) {
      error(createErrorEntity(e));
    }
  }
  // 请求，返回参数为 List
  // method：请求方法，NWMethod.POST等
  // path：请求地址
  // params：请求参数
  // success：请求成功回调
  // error：请求失败回调
  Future requestList<T>(NWMethod method, String path,
      {Map params,
      Function(List<T>) success,
      Function(ErrorEntity) error}) async {
    try {
      if (params == null) {
        params = new Map();
      }
      params['projectCode'] = "MSYX";
      params['projectId'] = "1";

      Map<String, dynamic> headers = new Map();
//      headers['Response headers'] = "*";
//      headers['Access-Control-Allow-Headers'] = "true";

      Response response = await dio.request(path,
          data: params, options: Options(method: NWMethodValues[method],headers: headers));
      if (response != null) {
        BaseListEntity entity = BaseListEntity<T>.fromJson(response.data);
        if (entity.status == 200) {
          success(entity.data);
        } else {
          error(ErrorEntity(status: entity.status, message: entity.message));
        }
      } else {
        error(ErrorEntity(status: -1, message: "未知错误"));
      }
    } on DioError catch (e) {
      error(createErrorEntity(e));
    }
  }

  Future requestOldList<T>(NWMethod method, String path,
      {Map params,
        Function(List<T>) success,
        Function(ErrorEntity) error}) async {
    try {
      if (params == null) {
        params = new Map();
      }
      params['projectCode'] = "MSYX";
      params['projectId'] = "1";

      Map<String, dynamic> headers = new Map();
//      headers['Response headers'] = "*";
//      headers['Access-Control-Allow-Headers'] = "true";

      Response response = await dio.request(path,
          data: params, options: Options(method: NWMethodValues[method],headers: headers));
      if (response != null) {
        OldBaseListEntity entity = OldBaseListEntity<T>.fromJson(response.data);
        if (entity.status == 200) {
          success(entity.result);
        } else {
          error(ErrorEntity(status: entity.status, message: entity.message));
        }
      } else {
        error(ErrorEntity(status: -1, message: "未知错误"));
      }
    } on DioError catch (e) {
      error(createErrorEntity(e));
    }
  }

  // 错误信息
  ErrorEntity createErrorEntity(DioError error) {
    switch (error.type) {
      case DioErrorType.CANCEL:
        {
          return ErrorEntity(status: -1, message: "请求取消");
        }
        break;
      case DioErrorType.CONNECT_TIMEOUT:
        {
          return ErrorEntity(status: -1, message: "连接超时");
        }
        break;
      case DioErrorType.SEND_TIMEOUT:
        {
          return ErrorEntity(status: -1, message: "请求超时");
        }
        break;
      case DioErrorType.RECEIVE_TIMEOUT:
        {
          return ErrorEntity(status: -1, message: "响应超时");
        }
        break;
      case DioErrorType.RESPONSE:
        {
          try {
            int errCode = error.response.statusCode;
            String errMsg = error.response.statusMessage;
            return ErrorEntity(status: errCode, message: errMsg);
//     switch (errCode) {
//      case 400: {
//       return ErrorEntity(code: errCode, message: "请求语法错误");
//      }
//      break;
//      case 403: {
//       return ErrorEntity(code: errCode, message: "服务器拒绝执行");
//      }
//      break;
//      case 404: {
//       return ErrorEntity(code: errCode, message: "无法连接服务器");
//      }
//      break;
//      case 405: {
//       return ErrorEntity(code: errCode, message: "请求方法被禁止");
//      }
//      break;
//      case 500: {
//       return ErrorEntity(code: errCode, message: "服务器内部错误");
//      }
//      break;
//      case 502: {
//       return ErrorEntity(code: errCode, message: "无效的请求");
//      }
//      break;
//      case 503: {
//       return ErrorEntity(code: errCode, message: "服务器挂了");
//      }
//      break;
//      case 505: {
//       return ErrorEntity(code: errCode, message: "不支持HTTP协议请求");
//      }
//      break;
//      default: {
//       return ErrorEntity(code: errCode, message: "未知错误");
//      }
//     }
          } on Exception catch (_) {
            return ErrorEntity(status: -1, message: "未知错误");
          }
        }
        break;
      default:
        {
          return ErrorEntity(status: -1, message: error.message);
        }
    }
  }
}

enum NWMethod { GET, POST, DELETE, PUT }
//使用：NWMethodValues[NWMethod.POST]
const NWMethodValues = {
  NWMethod.GET: "get",
  NWMethod.POST: "post",
  NWMethod.DELETE: "delete",
  NWMethod.PUT: "put"
};
