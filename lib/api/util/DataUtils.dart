import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

class DataUtils {
  static const String IS_LOGIN = "isLogin";
  static const String USERNAME = "userId";
  static const String TOKEN = "token";
  // 保存用户登录信息，data中包含了userName
  static Future saveLoginInfo(String userId,String token) async {
    print('isLogin');
    SharedPreferences sp = await SharedPreferences.getInstance();
    await sp.setString(USERNAME, userId);
    await sp.setString(TOKEN, token);
    await sp.setBool(IS_LOGIN, true);
  }

  static Future clearLoginInfo() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    print('clean');
    return sp.clear();
  }


  static Future<String> getUserName() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    return sp.getString(USERNAME);
  }
  static Future<String> getToken() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    return sp.getString(TOKEN);
  }

  static Future<bool> isLogin() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    bool b = sp.getBool(IS_LOGIN);
    return true == b;
  }
}
