class Api {
//  static const String BASE_API ="http://localhost:64100/";

  static const String BASE_API = "https://app-api.medsci.cn/";

  //首页上部分(包含轮播广告,工具列表)
  static const String NEW_HOME_ABOVE_URL = "home/home-above";

  //首页 推荐列表
  static const String NEW_HOME_LIST_DATA = "home/home-feed-list";

  //根据文章id获取详情
  static const String NEW_ARTICLE_DETAIL = BASE_API + "article/detail/";

  //推荐信息
  static const String RECOMMEND_INFO =
      BASE_API + "app-info/getRecommendInfoPage";

  //分类信息
  static const String CATEGORY_INFO =
      BASE_API + "app-info/getInfoListByCategoryId";

  //获取App个人信息
  static const String GET_USER_INFO =
      BASE_API + "app-medsciUser/getAppUserInfo";
  static const String LOGIN_ACCOUNT = BASE_API + "app-medsciUserAuth/medsciUserFrontLogin";
  //获取临床实践列表
  static const String LIST_VIDEO_SELECT =
      "http://api.center.medsci.cn/medsci-edu/list/index?";


  static const String VIDEO_DATA_TOTAL = "http://api.center.medsci.cn/medsci-edu/course/show-details/";


}
