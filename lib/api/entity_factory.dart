import 'package:flutterapp/data/ContentInfo.dart';
import 'package:flutterapp/data/HomeContentBean.dart';
import 'package:flutterapp/data/LoginInfo.dart';
import 'package:flutterapp/data/NewNewsInfo.dart';
import 'package:flutterapp/data/UserInfo.dart';
import 'package:flutterapp/data/VideoListInfo.dart';
import 'package:flutterapp/data/home_aboveBean.dart';

class EntityFactory {
  static T generateOBJ<T>(json) {
    if (json == null) {
      return null;
    }
//可以在这里加入任何需要并且可以转换的类型，例如下面
    else if (T.toString() == "home_aboveBean") {
      return home_aboveBean.fromJson(json) as T;
    } else if (T.toString() == "HomeContentBean") {
      return HomeContentBean.fromJson(json) as T;
    } else if (T.toString() == "ContentInfo") {
      return ContentInfo.fromJson(json) as T;
    } else if (T.toString() == "NewNewsInfo") {
      return NewNewsInfo.fromJson(json) as T;
    } else if (T.toString() == "VideoListInfo") {
      return VideoListInfo.fromJson(json) as T;
    } else if (T.toString() == "Video") {
      return Video.fromJson(json) as T;
    } else if (T.toString() == "LoginInfo") {
      return LoginInfo.fromJson(json) as T;
    } else if (T.toString() == "UserInfo") {
      return UserInfo.fromJson(json) as T;
    } else {
      return json as T;
    }
  }
}

class BaseEntity<T> {
  int status;
  String message;
  T data;
  int totalSize;

  BaseEntity({this.status, this.message, this.data, this.totalSize});

  factory BaseEntity.fromJson(json) {
    return BaseEntity(
      status: json["status"],
      message: json["message"],
      // data值需要经过工厂转换为我们传进来的类型
      data: EntityFactory.generateOBJ<T>(json["data"]),
      totalSize: json["totalSize"] == null ? 0 : json["totalSize"],
    );
  }
}

class OldBaseEntity<T> {
  int status;
  String message;
  T result;

  OldBaseEntity({this.status, this.message, this.result});

  factory OldBaseEntity.fromJson(json) {
    return OldBaseEntity(
      status: json["status"],
      message: json["message"],
      // data值需要经过工厂转换为我们传进来的类型
      result: EntityFactory.generateOBJ<T>(json["result"]),
    );
  }
}

class OldBaseListEntity<T> {
  int status;
  String message;
  List<T> result;

  OldBaseListEntity({this.status, this.message, this.result});

  factory OldBaseListEntity.fromJson(json) {
    List<T> mData = List();
    if (json['result'] != null) {
      //遍历data并转换为我们传进来的类型
      (json['result'] as List).forEach((v) {
        mData.add(EntityFactory.generateOBJ<T>(v));
      });
    }

    return OldBaseListEntity(
      status: json["status"],
      message: json["message"],
      result: mData,
    );
  }
}

class BaseListEntity<T> {
  int status;
  String message;
  List<T> data;

  BaseListEntity({this.status, this.message, this.data});

  factory BaseListEntity.fromJson(json) {
    List<T> mData = List();
    if (json['data'] != null) {
      //遍历data并转换为我们传进来的类型
      (json['data'] as List).forEach((v) {
        mData.add(EntityFactory.generateOBJ<T>(v));
      });
    }

    return BaseListEntity(
      status: json["status"],
      message: json["message"],
      data: mData,
    );
  }
}

class ErrorEntity {
  int status;
  String message;

  ErrorEntity({this.status, this.message});
}
