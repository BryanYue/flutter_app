class NewNewsInfo{
  int id;
  int projectId;
  String moduleId;
  String module;
  String chTitle;
  String cover;
  int authorId;
  String author;
  String summary;
  String source;
  String tags;
  String publishedTime;
  String publishedTimeString;
  String createdBy;
  String createdName;

  NewNewsInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    projectId = json['projectId'];
    moduleId = json['moduleId'];
    module = json['module'];
    chTitle = json['chTitle'];
    cover = json['cover'];
    authorId = json['authorId'];
    author = json['author'];
    summary = json['summary'];
    source = json['source'];
    tags = json['tags'];
    publishedTime = json['publishedTime'];
    publishedTimeString = json['publishedTimeString'];
    createdBy = json['createdBy'];
    createdName = json['createdName'];
  }
}