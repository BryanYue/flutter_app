class LoginInfo{
  int projectId;
  String projectName;
  String projectCode;
  int projectType;
  int projectUserStatus;
  String userId;
  String userName;
  String realName;
  Null ava6tar;
  String email;
  String mobile;
  String wechatOpenid;
  Null qqOpenid;
  int userStatus;
  List<GetRoleLoginResponses> getRoleLoginResponses;
  Token token;
  int count;

  LoginInfo.fromJson(Map<String, dynamic> json) {
    projectId = json['projectId'];
    projectName = json['projectName'];
    projectCode = json['projectCode'];
    projectType = json['projectType'];
    projectUserStatus = json['projectUserStatus'];
    userId = json['userId'];
    userName = json['userName'];
    realName = json['realName'];
    ava6tar = json['ava6tar'];
    email = json['email'];
    mobile = json['mobile'];
    wechatOpenid = json['wechatOpenid'];
    qqOpenid = json['qqOpenid'];
    userStatus = json['userStatus'];
    if (json['getRoleLoginResponses'] != null) {
      getRoleLoginResponses = new List<GetRoleLoginResponses>();
      json['getRoleLoginResponses'].forEach((v) {
        getRoleLoginResponses.add(new GetRoleLoginResponses.fromJson(v));
      });
    }
    token = json['token'] != null ? new Token.fromJson(json['token']) : null;
    count = json['count'];
  }
}
class GetRoleLoginResponses {
  int roleId;
  String roleName;
  Null roleEnglishName;
  int identityFlag;
  Null roleGrade;

  GetRoleLoginResponses(
      {this.roleId,
        this.roleName,
        this.roleEnglishName,
        this.identityFlag,
        this.roleGrade});

  GetRoleLoginResponses.fromJson(Map<String, dynamic> json) {
    roleId = json['roleId'];
    roleName = json['roleName'];
    roleEnglishName = json['roleEnglishName'];
    identityFlag = json['identityFlag'];
    roleGrade = json['roleGrade'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['roleId'] = this.roleId;
    data['roleName'] = this.roleName;
    data['roleEnglishName'] = this.roleEnglishName;
    data['identityFlag'] = this.identityFlag;
    data['roleGrade'] = this.roleGrade;
    return data;
  }
}

class Token {
  String accessToken;
  String refreshToken;
  int accessTokenExpireTime;

  Token({this.accessToken, this.refreshToken, this.accessTokenExpireTime});

  Token.fromJson(Map<String, dynamic> json) {
    accessToken = json['accessToken'];
    refreshToken = json['refreshToken'];
    accessTokenExpireTime = json['accessTokenExpireTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accessToken'] = this.accessToken;
    data['refreshToken'] = this.refreshToken;
    data['accessTokenExpireTime'] = this.accessTokenExpireTime;
    return data;
  }
}