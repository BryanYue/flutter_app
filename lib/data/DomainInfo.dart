class DomainInfo {
  bool isSubscription = false;
  int id;
  int pid;
  String code;
  String titleCn;
  String titleEn;
  int sort;
  List<TransverseCategory> transverseCategory;

  DomainInfo(this.id, this.titleCn);

  DomainInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    pid = json['pid'];
    code = json['code'];
    titleCn = json['titleCn'];
    titleEn = json['titleEn'];
    sort = json['sort'];
    transverseCategory = (json['transverseCategory'] as List)
        ?.map((e) => e == null
            ? null
            : new TransverseCategory.fromJson(e as Map<String, dynamic>))
        ?.toList();
  }
}

class TransverseCategory {
  int categoryId;
  String titleCn;

  TransverseCategory.fromJson(Map<String, dynamic> json) {
    categoryId = json['categoryId'];
    titleCn = json['titleCn'];
  }
}

class ChannelItem {
  String name;
  int list_id;

  ChannelItem(
    this.list_id,
    this.name,
  );

  ChannelItem.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    list_id = json['list_id'];
  }
}
