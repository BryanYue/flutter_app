import 'dart:convert';

class HomeContentBean {
  String module;
  String moduleId;
  dynamic attach;

  HomeContentBean(this.module, this.moduleId, this.attach);

  HomeContentBean.fromJson(Map<String, dynamic> json)
      : module = json['module'],
        moduleId = json['moduleId'],
        attach = json['attach'];
}

class tool_impact_factor {
  String abbr;
  String cover;
  double medsciHotlight;
  double submissionToAcceptanceFloat;
  double acceptanceRate;
  int submitNum;

  tool_impact_factor(this.abbr, this.cover, this.medsciHotlight,
      this.submissionToAcceptanceFloat, this.acceptanceRate, this.submitNum);

  tool_impact_factor.fromJson(Map<String, dynamic> json)
      : abbr = json['abbr'],
        cover = json['cover'],
        medsciHotlight = json['medsciHotlight'],
        submissionToAcceptanceFloat = json['submissionToAcceptanceFloat']== null? 0:json['submissionToAcceptanceFloat'],
        acceptanceRate = json['acceptanceRate']== null? 0:json['acceptanceRate'],
        submitNum = json['submitNum']== null? 0:json['submitNum'];
}

class article {
  String title;
  String cover;
  String source;
  String author;
  int authorId;
  String publishedTime;
  String publishedTimeString;

  article(this.title, this.cover, this.source, this.author, this.authorId,
      this.publishedTime, this.publishedTimeString);

  article.fromJson(dynamic json)
      : title = json['title'],
        cover = json['cover'],
        source = json['source'],
        author = json['author'],
        authorId = json['authorId'],
        publishedTime = json['publishedTime'],
        publishedTimeString = json['publishedTimeString'];
}

class advertisement {
  String adName;
  String adTags;
  String adUrl;
  String shareUrl;
  String adImage;
  String startAt;

  advertisement(this.adName, this.adTags, this.adUrl, this.shareUrl,
      this.adImage, this.startAt);

  advertisement.fromJson(dynamic json)
      : adName = json['adName'],
        adTags = json['adTags'],
        adUrl = json['adUrl'],
        shareUrl = json['shareUrl'],
        adImage = json['adImage'],
        startAt = json['startAt'];
}

class guider {
  String title;
  String cover;
  String source;
  String author;
  int authorId;
  String publishedTime;
  String publishedTimeString;

  guider(this.title, this.cover, this.source, this.author, this.authorId,
      this.publishedTime, this.publishedTimeString);

  guider.fromJson(dynamic json)
      : title = json['title'],
        cover = json['cover'],
        source = json['source'],
        author = json['author'],
        authorId = json['authorId'],
        publishedTime = json['publishedTime'],
        publishedTimeString = json['publishedTimeString'];
}
