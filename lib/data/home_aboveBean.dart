import 'dart:convert';

class home_aboveBean {
  List<AdListBean> adList;
  List<ToolListBean> toolList;

  home_aboveBean.fromJson(Map<String, dynamic> json)
      : adList = (json['adList'] as List)
            ?.map((e) => e == null
                ? null
                : new AdListBean.fromJson(e as Map<String, dynamic>))
            ?.toList(),
         toolList = (json['toolList'] as List)?.map((e) => e == null ? null : new ToolListBean.fromJson(e as Map<String, dynamic>))?.toList();
}

class AdListBean {
  int id;
  String adName;
  String adTags;
  String adUrl;
  String adType;
  String adShareUrl;
  String adImage;
  String startAt;

  AdListBean(this.id, this.adName, this.adTags, this.adUrl, this.adType,
      this.adShareUrl, this.adImage, this.startAt);

  AdListBean.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        adName = json['adName'],
        adTags = json['adTags'],
        adUrl = json['adUrl'],
        adType = json['adType'],
        adShareUrl = json['adShareUrl'],
        adImage = json['adImage'],
        startAt = json['startAt'];
}

class ToolListBean {
  int id;
  String name;
  int sort;
  String url;
  String pic;
  int isLogin;
  int isFixed;

  bool isSelect = false;
  bool isTitle = false;

  ToolListBean.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        sort = json['sort'],
        url = json['url'],
        pic = json['pic'],
        isLogin = json['isLogin'],
        isFixed = json['isFixed'];
}
