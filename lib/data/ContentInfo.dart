class ContentInfo {
  Article article;
  List<RelationArticleList> relationArticleList;
  List<ExpandReadArticleList> expandReadArticleList;

  ContentInfo.fromJson(Map<String, dynamic> json)
      : relationArticleList = (json['relationArticleList'] as List)
            ?.map((e) => e == null
                ? null
                : new RelationArticleList.fromJson(e as Map<String, dynamic>))
            ?.toList(),
        expandReadArticleList = (json['expandReadArticleList'] as List)
            ?.map((e) => e == null
                ? null
                : new ExpandReadArticleList.fromJson(e as Map<String, dynamic>))
            ?.toList(),
        article = json['article'] == null
            ? null
            : new Article.fromJson(json['article']);
}

class Article {
  String id;
  int projectId;
  Null sourceId;
  String title;
  String articleFrom;
  String journalId;
  String copyright;
  List<int> creationTypeList;
  String summary;
  String cover;
  int authorId;
  String author;
  String originalUrl;
  String linkOutUrl;
  String content;
  String belongTo;
  List<TagList> tagList;
  List<CategoryList> categoryList;
  int articleKeywordId;
  String articleKeyword;
  int articleKeywordNum;
  int guiderKeywordId;
  String guiderKeyword;
  int guiderKeywordNum;
  int opened;
  String paymentType;
  String paymentAmount;
  int recommend;
  Null recommendEndTime;
  int sticky;
  Null stickyEndTime;
  int allHits;
  int appHits;
  int showAppHits;
  int pcHits;
  int showPcHits;
  int likes;
  int shares;
  int comments;
  int approvalStatus;
  String publishedTime;
  String publishedTimeString;
  int pcVisible;
  int appVisible;
  int editorId;
  String editor;
  int waterMark;
  int formatted;
  int deleted;
  int version;
  String createdBy;
  String createdName;
  String createdTime;
  int updatedBy;
  String updatedName;
  String updatedTime;

  Article(
      {this.id,
      this.projectId,
      this.sourceId,
      this.title,
      this.articleFrom,
      this.journalId,
      this.copyright,
      this.creationTypeList,
      this.summary,
      this.cover,
      this.authorId,
      this.author,
      this.originalUrl,
      this.linkOutUrl,
      this.content,
      this.belongTo,
      this.tagList,
      this.categoryList,
      this.articleKeywordId,
      this.articleKeyword,
      this.articleKeywordNum,
      this.guiderKeywordId,
      this.guiderKeyword,
      this.guiderKeywordNum,
      this.opened,
      this.paymentType,
      this.paymentAmount,
      this.recommend,
      this.recommendEndTime,
      this.sticky,
      this.stickyEndTime,
      this.allHits,
      this.appHits,
      this.showAppHits,
      this.pcHits,
      this.showPcHits,
      this.likes,
      this.shares,
      this.comments,
      this.approvalStatus,
      this.publishedTime,
      this.publishedTimeString,
      this.pcVisible,
      this.appVisible,
      this.editorId,
      this.editor,
      this.waterMark,
      this.formatted,
      this.deleted,
      this.version,
      this.createdBy,
      this.createdName,
      this.createdTime,
      this.updatedBy,
      this.updatedName,
      this.updatedTime});

  Article.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    projectId = json['projectId'];
    sourceId = json['sourceId'];
    title = json['title'];
    articleFrom = json['articleFrom'];
    journalId = json['journalId'];
    copyright = json['copyright'];
    if (json['creationTypeList'] != null) {
      creationTypeList = json['creationTypeList'].cast<int>();
    }
    summary = json['summary'];
    cover = json['cover'];
    authorId = json['authorId'];
    author = json['author'];
    originalUrl = json['originalUrl'];
    linkOutUrl = json['linkOutUrl'];
    content = json['content'];
    belongTo = json['belongTo'];
    if (json['tagList'] != null) {
      tagList = new List<TagList>();
      json['tagList'].forEach((v) {
        tagList.add(new TagList.fromJson(v));
      });
    }
    if (json['categoryList'] != null) {
      categoryList = new List<CategoryList>();
      json['categoryList'].forEach((v) {
        categoryList.add(new CategoryList.fromJson(v));
      });
    }
    articleKeywordId = json['articleKeywordId'];
    articleKeyword = json['articleKeyword'];
    articleKeywordNum = json['articleKeywordNum'];
    guiderKeywordId = json['guiderKeywordId'];
    guiderKeyword = json['guiderKeyword'];
    guiderKeywordNum = json['guiderKeywordNum'];
    opened = json['opened'];
    paymentType = json['paymentType'];
    paymentAmount = json['paymentAmount'];
    recommend = json['recommend'];
    recommendEndTime = json['recommendEndTime'];
    sticky = json['sticky'];
    stickyEndTime = json['stickyEndTime'];
    allHits = json['allHits'];
    appHits = json['appHits'];
    showAppHits = json['showAppHits'];
    pcHits = json['pcHits'];
    showPcHits = json['showPcHits'];
    likes = json['likes'];
    shares = json['shares'];
    comments = json['comments'];
    approvalStatus = json['approvalStatus'];
    publishedTime = json['publishedTime'];
    publishedTimeString = json['publishedTimeString'];
    pcVisible = json['pcVisible'];
    appVisible = json['appVisible'];
    editorId = json['editorId'];
    editor = json['editor'];
    waterMark = json['waterMark'];
    formatted = json['formatted'];
    deleted = json['deleted'];
    version = json['version'];
    createdBy = json['createdBy'];
    createdName = json['createdName'];
    createdTime = json['createdTime'];
    updatedBy = json['updatedBy'];
    updatedName = json['updatedName'];
    updatedTime = json['updatedTime'];
  }
}

class TagList {
  int tagId;
  String tagName;

  TagList({this.tagId, this.tagName});

  TagList.fromJson(Map<String, dynamic> json) {
    tagId = json['tagId'];
    tagName = json['tagName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tagId'] = this.tagId;
    data['tagName'] = this.tagName;
    return data;
  }
}

class CategoryList {
  int categoryId;
  String categoryName;

  CategoryList({this.categoryId, this.categoryName});

  CategoryList.fromJson(Map<String, dynamic> json) {
    categoryId = json['categoryId'];
    categoryName = json['categoryName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['categoryId'] = this.categoryId;
    data['categoryName'] = this.categoryName;
    return data;
  }
}

class RelationArticleList {
  String id;
  String title;
  String cover;
  String summary;
  String author;
  String publishedTime;

  RelationArticleList(
      {this.id,
      this.title,
      this.cover,
      this.summary,
      this.author,
      this.publishedTime});

  RelationArticleList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    cover = json['cover'];
    summary = json['summary'];
    author = json['author'];
    publishedTime = json['publishedTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['cover'] = this.cover;
    data['summary'] = this.summary;
    data['author'] = this.author;
    data['publishedTime'] = this.publishedTime;
    return data;
  }
}

class ExpandReadArticleList {
  String id;
  String title;
  String cover;
  String summary;
  String type;
  String author;
  String publishedTime;

  ExpandReadArticleList(this.id, this.title, this.cover, this.summary,
      this.type, this.author, this.publishedTime);

  ExpandReadArticleList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    cover = json['cover'];
    summary = json['summary'];
    type = json['type'];
    author = json['author'];
    publishedTime = json['publishedTime'];
  }
}
