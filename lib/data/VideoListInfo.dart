

class VideoListInfo {
  List<VideoList> list;

//  List<Department> department;

  VideoListInfo.fromJson(Map<String, dynamic> json) {
    if (json['list'] != null) {
      list = new List<VideoList>();
      json['list'].forEach((v) {
        list.add(new VideoList.fromJson(v));
      });
    }
//    if (json['department'] != null) {
//      department = new List<Department>();
//      json['department'].forEach((v) {
//        department.add(new Department.fromJson(v));
//      });
//    }
  }
}

class VideoList {
  int id;
  String title;
  String subject;
  String tags;
  String description;
  String score;
  String path;
  String pics;
  String videoLength;
  String sections;
  String speakerUid;
  String speakerName;
  String speakerDes;
  int series;
  int counterView;
  String counterRegister;
  String counterLearned;
  String counterComment;
  String counterCommentScore;
  String contentIntro;
  String timeRegister;
  String timeRegisterEnd;
  String timeStart;
  String createTime;
  String updateTime;
  String status;
  String isDelete;
  String seriestitle;
  String difficulty;
  String isOpen;
  String realCounterView;
  String collectHits;
  String shareHits;
  String sponsor;
  String userHits;
  String courseType;
  String isCourseShop;
  String courseShopPrice;
  String courseShopFavorablePrice;
  String isCourseFavorableShop;
  String sales;
  String speaker_name;
  String speakerTitle;
  String speakerDepartment;
  String speakerUnit;

  VideoList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    subject = json['subject'];
    tags = json['tags'];
    description = json['description'];
    score = json['score'];
    path = json['path'];
    pics = json['pics'];
    videoLength = json['video_length'];
    sections = json['sections'];
    speakerUid = json['speaker_uid'];
    speaker_name = json['speaker_name'];
    speakerDes = json['speaker_des'];
    series = json['series'];
    counterView = json['counter_view'];
    counterRegister = json['counter_register'];
    counterLearned = json['counter_learned'];
    counterComment = json['counter_comment'];
    counterCommentScore = json['counter_comment_score'];
    contentIntro = json['content_intro'];
    timeRegister = json['time_register'];
    timeRegisterEnd = json['time_register_end'];
    timeStart = json['time_start'];
    createTime = json['create_time'];
    updateTime = json['update_time'];
    status = json['status'];
    isDelete = json['is_delete'];
    seriestitle = json['seriestitle'];
    difficulty = json['difficulty'];
    isOpen = json['is_open'];
    realCounterView = json['real_counter_view'];
    collectHits = json['collect_hits'];
    shareHits = json['share_hits'];
    sponsor = json['sponsor'];
    userHits = json['user_hits'];
    courseType = json['course_type'];
    isCourseShop = json['is_course_shop'];
    courseShopPrice = json['course_shop_price'];
    courseShopFavorablePrice = json['course_shop_favorable_price'];
    isCourseFavorableShop = json['is_course_favorable_shop'];
    sales = json['sales'];
    speakerName = json['speakerName'];
    speakerTitle = json['speaker_title'];
    speakerDepartment = json['speaker_department'];
    speakerUnit = json['speaker_unit'];
  }
}

//class Department {
//  dynamic classId;
//  String name;
//
//  Department.fromJson(Map<String, dynamic> json) {
//    if (json['class_id'] as int != null) {
//      int class_id = json['class_id'];
//      classId = class_id.toString();
//    } else {
//      classId = json['class_id'];
//    }
//
//    name = json['name'];
//  }
//}

class Video{
  int id;
  String shareHits;
  String collectHits;
  String title;
  String pics;
  String contentIntro;
  String path;
  String series;
  String timeStart;
  String seriestitle;
  String score;
  String description;
  String videoLength;
  int speakerUid;
  String counterView;
  List<dynamic> sponsor;
  String videoPath;
  List<Course2category> course2category;
  String alreadyCollect;
  String speakerPhoto;
  String department;
  String speakerDescription;
  String speakerName;
  String speakerTitle;
  String unit;
  bool isBuy;
  List<SeriesList> seriesList;
  SeriesArr seriesArr;
  String commentCount;
  List<ChapterList> chapterList;
  String certificateState;

  Video.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shareHits = json['share_hits'];
    collectHits = json['collect_hits'];
    title = json['title'];
    pics = json['pics'];
    contentIntro = json['content_intro'];
    path = json['path'];
    series = json['series'];
    timeStart = json['time_start'];
    seriestitle = json['seriestitle'];
    score = json['score'];
    description = json['description'];
    videoLength = json['video_length'];
    speakerUid = json['speaker_uid'];
    counterView = json['counter_view'];
    if (json['sponsor'] != null) {
      sponsor = new List<dynamic>();
      json['sponsor'].forEach((v) {
        sponsor.add(v);
      });
    }
    videoPath = json['video_path'];
    if (json['course2category'] != null) {
      course2category = new List<Course2category>();
      json['course2category'].forEach((v) {
        course2category.add(new Course2category.fromJson(v));
      });
    }
    alreadyCollect = json['alreadyCollect'];
    speakerPhoto = json['speaker_photo'];
    department = json['department'];
    speakerDescription = json['speaker_description'];
    speakerName = json['speaker_name'];
    speakerTitle = json['speaker_title'];
    unit = json['unit'];
    isBuy = json['is_buy'];
    if (json['series_list'] != null) {
      seriesList = new List<SeriesList>();
      json['series_list'].forEach((v) {
        seriesList.add(new SeriesList.fromJson(v));
      });
    }
    seriesArr = json['series_arr'] != null
        ? new SeriesArr.fromJson(json['series_arr'])
        : null;
    commentCount = json['comment_count'];
    if (json['chapter_list'] != null) {
      chapterList = new List<ChapterList>();
      json['chapter_list'].forEach((v) {
        chapterList.add(new ChapterList.fromJson(v));
      });
    }
    certificateState = json['certificate_state'];
  }
}
class Course2category {
  int id;
  String courseId;
  String categoryId;

  Course2category({this.id, this.courseId, this.categoryId});

  Course2category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    courseId = json['course_id'];
    categoryId = json['category_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['course_id'] = this.courseId;
    data['category_id'] = this.categoryId;
    return data;
  }
}

class SeriesList {
  String id;
  String title;
  String pics;
  String path;
  String timeStart;
  String seriestitle;
  String score;
  String counterView;
  String series;
  List<Category> category;
  bool isBuy;
  SeriesArr seriesArr;
  String speakerName;
  String speakerTitle;
  String speakerPhoto;
  String speakerDescription;
  String speakerUnit;
  String speakerDepartment;
  String speaker_name;



  SeriesList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    pics = json['pics'];
    path = json['path'];
    timeStart = json['time_start'];
    seriestitle = json['seriestitle'];
    score = json['score'];
    counterView = json['counter_view'];
    series = json['series'];
    if (json['category'] != null) {
      category = new List<Category>();
      json['category'].forEach((v) {
        category.add(new Category.fromJson(v));
      });
    }
    isBuy = json['is_buy'];
    seriesArr = json['series_arr'] != null
        ? new SeriesArr.fromJson(json['series_arr'])
        : null;
    speaker_name = json['speaker_name'];
    speakerTitle = json['speaker_title'];
    speakerPhoto = json['speaker_photo'];
    speakerDescription = json['speaker_description'];
    speakerUnit = json['speaker_unit'];
    speakerDepartment = json['speaker_department'];
    speakerName = json['speakerName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['pics'] = this.pics;
    data['path'] = this.path;
    data['time_start'] = this.timeStart;
    data['seriestitle'] = this.seriestitle;
    data['score'] = this.score;
    data['counter_view'] = this.counterView;
    data['series'] = this.series;
    if (this.category != null) {
      data['category'] = this.category.map((v) => v.toJson()).toList();
    }
    data['is_buy'] = this.isBuy;
    if (this.seriesArr != null) {
      data['series_arr'] = this.seriesArr.toJson();
    }
    data['speaker_name'] = this.speakerName;
    data['speaker_title'] = this.speakerTitle;
    data['speaker_photo'] = this.speakerPhoto;
    data['speaker_description'] = this.speakerDescription;
    data['speaker_unit'] = this.speakerUnit;
    data['speaker_department'] = this.speakerDepartment;
    data['speakerName'] = this.speakerName;
    return data;
  }
}

class Category {
  String name;
  Pivot pivot;

  Category({this.name, this.pivot});

  Category.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    if (this.pivot != null) {
      data['pivot'] = this.pivot.toJson();
    }
    return data;
  }
}

class Pivot {
  String courseId;
  String categoryId;

  Pivot({this.courseId, this.categoryId});

  Pivot.fromJson(Map<String, dynamic> json) {
    courseId = json['course_id'];
    categoryId = json['category_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['course_id'] = this.courseId;
    data['category_id'] = this.categoryId;
    return data;
  }
}

class SeriesArr {
  String id;
  String price;
  String orderStatus;

  SeriesArr({this.id, this.price, this.orderStatus});

  SeriesArr.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    price = json['price'];
    orderStatus = json['order_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['price'] = this.price;
    data['order_status'] = this.orderStatus;
    return data;
  }
}

class ChapterList {
  String chapterId;
  String chapterTitle;
  String chapterPath;
  String chapterLength;

  ChapterList(
      {this.chapterId,
        this.chapterTitle,
        this.chapterPath,
        this.chapterLength});

  ChapterList.fromJson(Map<String, dynamic> json) {
    chapterId = json['chapter_id'];
    chapterTitle = json['chapter_title'];
    chapterPath = json['chapter_path'];
    chapterLength = json['chapter_length'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['chapter_id'] = this.chapterId;
    data['chapter_title'] = this.chapterTitle;
    data['chapter_path'] = this.chapterPath;
    data['chapter_length'] = this.chapterLength;
    return data;
  }
}