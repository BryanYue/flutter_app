class UserInfo {
  UserCommon userCommonResponse;
  GetCertification getCertificationResponse;
  GetUnreadMsg getUnreadMsgResponse;
  GetCountInfo getCountInfoResponse;
  GetSignInfo getSignInfoResponse;

  UserInfo.fromJson(Map<String, dynamic> json) {
    userCommonResponse = json['userCommonResponse'] == null
        ? null
        : new UserCommon.fromJson(json['userCommonResponse']);
    getCertificationResponse = json['getCertificationResponse'] == null
        ? null
        : new GetCertification.fromJson(json['getCertificationResponse']);
    getUnreadMsgResponse = json['getUnreadMsgResponse'] == null
        ? null
        : new GetUnreadMsg.fromJson(json['getUnreadMsgResponse']);
    getCountInfoResponse = json['getCountInfoResponse'] == null
        ? null
        : new GetCountInfo.fromJson(json['getCountInfoResponse']);
    getSignInfoResponse = json['getSignInfoResponse'] == null
        ? null
        : new GetSignInfo.fromJson(json['getSignInfoResponse']);
  }
}

class UserCommon {
  String email;
  String mobile;
  String userName;
  String realName;
  int authenticateStatus;
  String avatar;
  String userId;
  int integralBase;
  String address;
  String briefIntroduction;
  String departmentId;
  String departmentName;
  String companyId;
  String companyName;
  String professionalCatId;
  String professionalCatName;
  String professionalId;
  String professionalName;
  String provinceId;
  String provinceName;
  String cityId;
  String cityName;
  String districtId;
  String districtName;
  int integralUme;
  int certificationId;

  UserCommon.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    mobile = json['mobile'];
    userName = json['userName'];
    realName = json['realName'];
    authenticateStatus = json['authenticateStatus'];
    avatar = json['avatar'];
    userId = json['userId'];
    integralBase = json['integralBase'];
    address = json['address'];
    briefIntroduction = json['briefIntroduction'];
    departmentId = json['departmentId'].toString();
    companyId = json['companyId'].toString();
    companyName = json['companyName'];
    professionalCatId = json['professionalCatId'].toString();
    professionalCatName = json['professionalCatName'];
    professionalId = json['professionalId'].toString();
    professionalName = json['professionalName'];
    provinceId = json['provinceId'].toString();
    provinceName = json['provinceName'];
    cityId = json['cityId'].toString();
    cityName = json['cityName'];
    districtId = json['districtId'].toString();
    districtName = json['districtName'];
    integralUme = json['integralUme'];
    certificationId = json['certificationId'];
  }
}

class GetCertification {
  int certificationId;
  int authenticateStatus;
  String professionalCatCode;
  int authMethod;
  String professionNumber;
  List<FileInfo> fileIdRequests;

  GetCertification.fromJson(Map<String, dynamic> json) {
    certificationId = json['certificationId'];
    authenticateStatus = json['authenticateStatus'];
    professionalCatCode = json['professionalCatCode'];
    authMethod = json['authMethod'];
    professionNumber = json['professionNumber'];
    if (json['fileIdRequests'] != null) {
      fileIdRequests = new List<FileInfo>();
      json['fileIdRequests'].forEach((v) {
        fileIdRequests.add(new FileInfo.fromJson(v));
      });
    }
  }
}

class FileInfo {
  String fileId;

  FileInfo.fromJson(Map<String, dynamic> json) {
    fileId = json['fileId'];
  }
}

class GetUnreadMsg {
  String count;

  GetUnreadMsg.fromJson(Map<String, dynamic> json) {
    count = json['count'];
  }
}

class GetCountInfo {
  int attentionNum;
  int fansNum;
  int messageNum;
  int browseNum;
  int likeNum;

  GetCountInfo.fromJson(Map<String, dynamic> json) {
    attentionNum = json['attentionNum'];
    fansNum = json['fansNum'];
    messageNum = json['messageNum'];
    browseNum = json['browseNum'];
    likeNum = json['likeNum'];
  }
}

class GetSignInfo {
  bool signStatus;
  int integralBase;

  GetSignInfo.fromJson(Map<String, dynamic> json) {
    signStatus = json['signStatus'];
    integralBase = json['integralBase'];
  }
}
