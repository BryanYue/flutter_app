import 'package:flutter/material.dart';
import 'package:flutter_fai_webview/flutter_fai_webview.dart';
import 'package:flutterapp/api/api.dart';
import 'package:flutterapp/api/http_util.dart';
import 'package:flutterapp/data/ContentInfo.dart';

//文章详情界面
class ArticleDetailPage extends StatefulWidget {
  final String id;

  ArticleDetailPage({
    Key key,
    @required this.id,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ArticleDetailPageState();
  }
}

class ArticleDetailPageState extends State<ArticleDetailPage> {
  ContentInfo info;
  String html;
  FaiWebViewWidget webViewWidget;

  @override
  void initState() {
    super.initState();
    getDetail();
    webViewWidget = FaiWebViewWidget(
      //webview 加载网页链接
      htmlData: html,
      //webview 加载信息回调
      callback: callBack,
      //输出日志
      isLog: true,
    );

  }

//
//  @override
//  void initState() {
//    super.initState();
//    getDetail();
//    flutterWebViewPlugin.onStateChanged.listen((state) {
//      debugPrint('state:_' + state.type.toString());
//      if (state.type == WebViewState.finishLoad) {
//        // 加载完成
//        setState(() {});
//      } else if (state.type == WebViewState.startLoad) {
//        setState(() {});
//      }
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('梅斯医学'),
        centerTitle: true,
      ),
      body: Container(
        child: webViewWidget,
      ),
    );
  }

  void getDetail() {
    DioManager().request<ContentInfo>(
        NWMethod.GET, Api.NEW_ARTICLE_DETAIL + widget.id, params: null,
        success: (data) {
      info = data;
      String content = info.article.content;
      int titlesize = 20;
      int timesize = 12;
      content = "<style> p,td,div {line-height:180%; color: rgb(94, 93, 93);}  hr{height:1px; border:none; " +
          "margin-bottom:10px; border-bottom:1px solid #f0f0f0; } #bioon_body img{ width:100% " +
          "!important;  " +
          "height:auto !important;}" +
          " #bioon_body a{word-wrap:break-word; max-width：300px;  color: #2383dd; } #bioon_title{ " +
          "font-size:" +
          titlesize.toString() +
          "px; line-height:150%; margin-bottom:5px; color:#212121;} #bioon_time{ font-size:" +
          timesize.toString() +
          "px; color:#B3B3B3;} .wapnone{ display:none;} </style>" +
          "" +
          content;

      String html = "<div id='bioon_body' >" + content + "</div>";
      String title = "<div id='bioon_title'>" + info.article.title + "</div>";
      String time1 =
          "<div id = 'bioon_time'>" + info.article.updatedTime + "来源";
      String time2 = info.article.author + "</div>";
      String header = title + time1 + time2;
      String ss = "<body>" + header + "<hr />" + html + "</body>";
      html = ss;
    }, error: (error) {
      print("error code = ${error.status}, massage = ${error.message}");
    });
  }

  callBack(int code, String msg, content) {
    //加载页面完成后 对页面重新测量的回调
    //这里没有使用到
    //当FaiWebViewWidget 被嵌套在可滑动的 widget 中，必须设置 FaiWebViewWidget 的高度
    //设置 FaiWebViewWidget 的高度 可通过在 FaiWebViewWidget 嵌套一层 Container 或者 SizeBox
    if (code == 201) {
      webViewWidget = content;
      print("webViewHeight " + webViewWidget.toString());
    } else {
      //其他回调
    }
    setState(() {
      print("回调：code[" + code.toString() + "]; msg[" + msg.toString() + "]");
    });
  }
}
