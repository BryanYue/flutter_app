import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/api/api.dart';
import 'package:flutterapp/api/http_util.dart';
import 'package:flutterapp/api/util/DataUtils.dart';
import 'package:flutterapp/api/util/constants.dart';
import 'package:flutterapp/api/util/login_event.dart';
import 'package:flutterapp/api/util/screen_utils.dart';
import 'package:flutterapp/data/LoginInfo.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LoginPageState();
  }
}

class LoginPageState extends State<StatefulWidget> {
  TextEditingController _nameController;
  TextEditingController _passwordContro;
  final FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
    _passwordContro = TextEditingController();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _passwordContro.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("登录"),
      ),
      body: buildView(),
    );
  }

  Widget buildView() {
    Row name = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: Container(
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            width: ScreenUtils.getInstance().getWidth(375.0) / 16 - 2,
            height: ScreenUtils.getInstance().getWidth(375.0) / 16 - 2,
            child: Image.asset('images/zhuce_tx.png', fit: BoxFit.fill),
          ),
        ),
        Container(
            margin: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 5.0),
            width: ScreenUtils.getInstance().getWidth(375.0) / 6 * 5,
            height: 40.0,
            child: TextField(
              controller: _nameController,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: '邮箱/手机号/用户名',
              ),
              onEditingComplete: () {
                print('onEditingComplete');
              },
              onChanged: (v) {
                print('onChanged:' + v);
              },
              onSubmitted: (v) {
                FocusScope.of(context).requestFocus(_focusNode);
                print('onSubmitted:' + v);
//                _nameController.clear();
              },
            )),
      ],
    );

    Row password = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: Container(
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            width: ScreenUtils.getInstance().getWidth(375.0) / 16 - 2,
            height: ScreenUtils.getInstance().getWidth(375.0) / 16 - 2,
            child: Image.asset('images/zhuce_password.png', fit: BoxFit.fill),
          ),
        ),
        Container(
            margin: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 5.0),
            width: ScreenUtils.getInstance().getWidth(375.0) / 6 * 5,
            height: 40.0,
            child: TextField(
              controller: _passwordContro,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: '密码',
              ),
              onEditingComplete: () {
                print('onEditingComplete');
              },
              onChanged: (v) {
                print('onChanged:' + v);
              },
              onSubmitted: (v) {
                FocusScope.of(context).requestFocus(_focusNode);
                print('onSubmitted:' + v);
//                _passwordContro.clear();
              },
            )),
      ],
    );

    Column column = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
          child: name,
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
          child: password,
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
          child: OutlineButton(
            onPressed: () {
              _toLoinClick();
            },
            child: Text("登录"),
            textColor: Colors.blueAccent,
            borderSide: BorderSide(color: Colors.blueAccent, width: 1),
          ),
        ),
      ],
    );

    return Card(
      elevation: 4.0,
      child: InkWell(
        child: column,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
      ),
    );
  }

  void _toLoinClick() {
    String name = _nameController.text;
    String password = _passwordContro.text;
    if (name.length == 0) {
      _showMessage('手机号不能为空');
      return;
    }
    if (password.length == 0) {
      _showMessage('密码不能为空');
      return;
    }
    Map<String, String> map = new Map();
    map['value'] = name;
    map['password'] = generateMd5(generateMd5(password).substring(8, 24));
    map['mobileCode'] = "86";
    print("password " + generateMd5(generateMd5(password).substring(8, 24)));
    print("3acf16259def65456fc2a68ab5e10d96");
    DioManager().request<LoginInfo>(NWMethod.POST, Api.LOGIN_ACCOUNT,
        params: map, success: (data) {
      if (data != null) {
        DataUtils.saveLoginInfo(data.userId, data.token.accessToken).then((value) {
          Constants.eventBus.fire(LoginEvent());
          if(context != null && FocusNode != null){
            FocusScope.of(context).requestFocus(FocusNode());
          }
          Navigator.of(this.context).pop();
        });
      }
    }, error: (error) {
      print("error code = ${error.status}, massage = ${error.message}");
    });
  }

  void _showMessage(String msg) {
    final snackBar = SnackBar(content: Text(msg));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  String generateMd5(String data) {
    return md5.convert(utf8.encode(data)).toString();
  }
}
