import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/api/api.dart';
import 'package:flutterapp/api/http_util.dart';
import 'package:flutterapp/api/util/screen_utils.dart';
import 'package:flutterapp/data/VideoListInfo.dart';
import 'package:flutterapp/ui/Videoplay.dart';

class VideoListPage extends StatefulWidget {
  final String id;

  VideoListPage(this.id);

  @override
  State<StatefulWidget> createState() {
    return new VideoListPageState();
  }
}

class VideoListPageState extends State<VideoListPage>
    with AutomaticKeepAliveClientMixin {
  int curPage = 0;
  int listTotalSize = 500;
  List<VideoList> list = new List();
  ScrollController _controller = ScrollController();

  @override
  bool get wantKeepAlive => true;

  VideoListPageState() {
    _controller.addListener(() {
      var maxScroll = _controller.position.maxScrollExtent;
      var pixels = _controller.position.pixels;
      if (maxScroll == pixels && list.length < listTotalSize) {
        getVideolist();
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getVideolist();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (list == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      Widget listView = ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, i) => buildItem(i),
        controller: _controller,
      );

      return RefreshIndicator(child: listView, onRefresh: _pullToRefresh);
    }
  }

  Future<Null> _pullToRefresh() async {
    curPage = 0;
    getVideolist();
    return null;
  }

  void getVideolist() {
    String url = Api.LIST_VIDEO_SELECT +
        "class_id=" +
        widget.id +
        "&score=<1&orderby=create_time&page=" +
        curPage.toString();

    DioManager().requestOld<VideoListInfo>(NWMethod.GET, url, success: (data) {
      if (data != null) {
        setState(() {
          if (curPage == 0) {
            list.clear();
          }
          curPage++;

          list.addAll(data.list);
          print("data.length" + data.list.length.toString());
          print("list.length" + data.list.length.toString());
        });
      } else {
        print("data.length null");
      }
    }, error: (error) {
      print("error code = ${error.status}, massage = ${error.message}");
    });
  }

  Widget buildItem(int i) {
    if (i < list.length && i >= 0) {
      var itemData = list[i];
      if (itemData != null) {
        return buildVideo(itemData);
      }
    }
  }

  Widget buildVideo(VideoList item) {
    Row title = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 3,
          height: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1,
          child: CachedNetworkImage(
            imageUrl:
                item.pics == null ? 'images/img_loadfailure.png' : item.pics,
            errorWidget: (context, url, error) =>
                Image.asset('images/img_loadfailure.png', fit: BoxFit.contain),
            width: ScreenUtils.getInstance().getWidth(375.0) / 3,
            height: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1,
          ),
        ),
        Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 3,
          height: 90.0,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                child: Text(
                  item.title,
                  style: TextStyle(fontSize: 16.0, color: Colors.grey),
                  maxLines: 2,
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
                child: Text(
                  item.speakerUnit == null ? "" : item.speakerUnit,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 12.0, color: Colors.grey),
                  maxLines: 1,
                ),
              ),
            ],
          ),
        ),
      ],
    );

    return Card(
      elevation: 4.0,
      child: InkWell(
        child: title,
        onTap: () {
          _itemClick(item);
        },
      ),
    );
  }

  void _itemClick(VideoList itemData) async {
    print("_itemClick module" + itemData.title);
    print("_itemClick moduleId" + itemData.id.toString());

    await Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return new Videoplay(itemData.id.toString(), itemData.title);
    }));
  }
}
