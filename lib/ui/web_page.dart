import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

//文章详情界面
class WebPage extends StatefulWidget {
   String title;
   String url;
   BuildContext parentContext;
  WebPage({
    Key key,
    @required this.title,
    @required this.url,
    @required this.parentContext,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return WebPageState();
  }
}

class WebPageState extends State<WebPage> {
//  bool isLoad = true;
//
 final flutterWebViewPlugin = FlutterWebviewPlugin();


 @override
  void initState() {
    super.initState();
//    flutterWebViewPlugin.onDestroy.listen((_){
//      Navigator.of(widget.parentContext).pop();
//    });
//
//    flutterWebViewPlugin.onStateChanged.listen((state) {
//      debugPrint('state:_' + state.type.toString());
//
//    });
  }



  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: widget.url,
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      withZoom: false,
      withLocalStorage: true,
      withJavascript: true,
      userAgent: ";medsci_app",
    );
  }
}
