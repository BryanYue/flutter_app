import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutterapp/data/DomainInfo.dart';
import 'package:flutterapp/ui/ArticleListPage.dart';

class NewsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new NewsState();
  }
}

class NewsState extends State<NewsPage> with SingleTickerProviderStateMixin {
  List<DomainInfo> list = new List();

  List<Tab> tabs = new List();

  TabController _tabContro;

  @override
  void initState() {
    super.initState();
    Future<dynamic> loadString =
        DefaultAssetBundle.of(context).loadString("data/Domainjson.json");
    loadString.then((value) {
      setState(() {
        list.clear();
        tabs.clear();
        list.add(new DomainInfo(0, "推荐"));
        (json.decode(value) as List).forEach((v) {
          list.add(DomainInfo.fromJson(v));
        });
        for (DomainInfo value in list) {
          tabs.add(Tab(text: value.titleCn));
          print(value.titleCn);
        }
        _tabContro = TabController(length: list.length, vsync: this);
      });
    });
  }

  @override
  void dispose() {
    _tabContro.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (tabs.length > 0) {
      return DefaultTabController(
        length: list.length,
        child: Scaffold(
          appBar: _buildTabBar(),
          body: _buildTableBarView(),
        ),
      );
    } else {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  Widget _buildTabBar() => TabBar(
        onTap: (tab) => print(tab),
        labelStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        unselectedLabelStyle: TextStyle(fontSize: 16),
        isScrollable: true,
        controller: _tabContro,
        labelColor: Colors.blue,
        unselectedLabelColor: Colors.grey,
        tabs: tabs,
      );

  Widget _buildTableBarView() => TabBarView(
      controller: _tabContro,
      children: list.map((DomainInfo itemData) {
        return ArticleListPage(itemData);
      }).toList());
}
