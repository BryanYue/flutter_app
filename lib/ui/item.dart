import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/api/util/screen_utils.dart';
import 'package:flutterapp/data/HomeContentBean.dart';
import 'package:flutterapp/ui/article_detail_page.dart';
import 'package:flutterapp/ui/web_page.dart';

class ArticleItem extends StatefulWidget {
  HomeContentBean itemData;

  ArticleItem(HomeContentBean itemData) {
    this.itemData = itemData;
  }

  @override
  State<StatefulWidget> createState() {
    return ArticleItemState();
  }
}

class ArticleItemState extends State<ArticleItem> {
  @override
  Widget build(BuildContext context) {
    Widget view;
    switch (widget.itemData.module) {
      case "article":
        view = buildArticle(widget.itemData);
        break;
      case "guider":
        view = buildGuider(widget.itemData);
        break;
      case "advertisement":
        view = buildAdvertisement(widget.itemData);
        break;
      case "tool_impact_factor":
        view = buildTool_impact_factor(widget.itemData);
        break;
    }
    return view;
  }

  Widget buildArticle(HomeContentBean item) {
    article bean = article.fromJson(item.attach);
    Row title = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 3,
          height: 48.0,
          child: Text(
            bean.title,
            maxLines: 2,
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 16.0, color: Colors.black),
          ),
        ),
        Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1.5,
          height: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1,
          child: CachedNetworkImage(
            imageUrl:
                bean.cover == null ? 'images/img_loadfailure.png' : bean.cover,
            errorWidget: (context, url, error) =>
                Image.asset('images/img_loadfailure.png', fit: BoxFit.fill),
            width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1.5,
            height: 72,
          ),
        )
      ],
    );

    Row source = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 12.0),
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1.5,
          height: 15.0,
          child: Text(
            bean.source,
            style: TextStyle(fontSize: 12.0, color: Colors.grey),
            maxLines: 2,
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 12.0),
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1.5,
          height: 15.0,
          child: Text(
            bean.publishedTimeString,
            style: TextStyle(fontSize: 12.0, color: Colors.grey),
            maxLines: 2,
          ),
        ),
      ],
    );

    Column column = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: title,
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
          child: source,
        ),
      ],
    );
    return Card(
      elevation: 4.0,
      child: InkWell(
        child: column,
        onTap: () {
          _itemClick(widget.itemData);
        },
      ),
    );
  }

  Widget buildGuider(HomeContentBean item) {
    guider bean = guider.fromJson(item.attach);

    Row title = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 3,
          height: 48.0,
          child: Text(
            bean.title,
            maxLines: 2,
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 16.0, color: Colors.black),
          ),
        ),
        Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1.5,
          height: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1,
          child: CachedNetworkImage(
            imageUrl:
            bean.cover == null ? 'images/img_loadfailure.png' : bean.cover,
            errorWidget: (context, url, error) =>
                Image.asset('images/img_loadfailure.png', fit: BoxFit.fill),
          ),
        ),
      ],
    );

    Row source = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 12.0),
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1.5,
          height: 15.0,
          child: Text(
            bean.source,
            style: TextStyle(fontSize: 12.0, color: Colors.grey),
            maxLines: 2,
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 12.0),
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1.5,
          height: 15.0,
          child: Text(
            bean.publishedTimeString,
            style: TextStyle(fontSize: 12.0, color: Colors.grey),
            maxLines: 2,
          ),
        ),
      ],
    );

    Column column = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: title,
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
          child: source,
        ),
      ],
    );
    return Card(
      elevation: 4.0,
      child: InkWell(
        child: column,
        onTap: () {
          _itemClick(widget.itemData);
        },
      ),
    );
  }

  Widget buildTool_impact_factor(HomeContentBean item) {
    tool_impact_factor bean = tool_impact_factor.fromJson(item.attach);

    Row date = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
          height: 15.0,
          child: Text(
            "审稿周期:    ",
            style: TextStyle(fontSize: 12.0, color: Colors.grey),
            maxLines: 2,
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
          height: 15.0,
          child: Text(
            bean.submissionToAcceptanceFloat.toString() + "月    ",
            style: TextStyle(fontSize: 12.0, color: Colors.blue),
            maxLines: 2,
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
          height: 15.0,
          child: Text(
            "经验：  ",
            style: TextStyle(fontSize: 12.0, color: Colors.grey),
            maxLines: 2,
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
          height: 15.0,
          child: Text(
            bean.submitNum.toString() + "条    ",
            style: TextStyle(fontSize: 12.0, color: Colors.blue),
            maxLines: 2,
          ),
        ),
      ],
    );

    Row acceptance = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
          height: 15.0,
          child: Text(
            "命中率：  ",
            style: TextStyle(fontSize: 12.0, color: Colors.grey),
            maxLines: 2,
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
          height: 15.0,
          child: Text(
            bean.acceptanceRate.toString(),
            style: TextStyle(fontSize: 12.0, color: Colors.blue),
            maxLines: 2,
          ),
        ),
      ],
    );

    Column title = Column(children: <Widget>[
      Padding(
        padding: EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
        child: Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 3,
          height: 24.0,
          child: Text(
            bean.abbr == null ? '' : bean.abbr,
            maxLines: 2,
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 16.0, color: Colors.black),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
        child: Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 3,
          child: acceptance,
        ),
      ),
      Padding(
        padding: EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
        child: Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 3,
          child: date,
        ),
      ),
    ]);

    Row head = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        title,
        Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1.5,
          height: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1,
          child: CachedNetworkImage(
            imageUrl:
                bean.cover == null ? 'images/img_loadfailure.png' : bean.cover,
            errorWidget: (context, url, error) =>
                Image.asset('images/img_loadfailure.png', fit: BoxFit.fill),
          ),
        ),
      ],
    );

    Row source = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
          height: 15.0,
          child: Text(
            "MedSci指数:    ",
            style: TextStyle(fontSize: 12.0, color: Colors.grey),
            maxLines: 1,
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
          height: 15.0,
          child: Text(
            bean.medsciHotlight.toString(),
            style: TextStyle(fontSize: 12.0, color: Colors.blue),
            maxLines: 1,
          ),
        ),
      ],
    );

    Column column = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: head,
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: source,
        ),
      ],
    );
    return Card(
      elevation: 4.0,
      child: InkWell(
        child: column,
        onTap: () {
          _itemClick(widget.itemData);
        },
      ),
    );
  }

  Widget buildAdvertisement(HomeContentBean item) {
    advertisement bean = advertisement.fromJson(item.attach);

    Row source = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          height: 15.0,
          child: MaterialButton(
            elevation: 1,
            minWidth: 48,
            onPressed: null,
            textColor: Colors.orange,
            highlightColor: Colors.orange,
            highlightElevation: 1,
            child: Text(
              "专题",
              style: TextStyle(fontSize: 12.0, color: Colors.orange),
              maxLines: 1,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          height: 15.0,
          child: Text(
            bean.startAt,
            style: TextStyle(fontSize: 12.0, color: Colors.black),
            maxLines: 1,
          ),
        ),
      ],
    );

    Column column = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: Container(
            width: ScreenUtils.getInstance().getWidth(375.0) - 30,
            height: 24.0,
            child: Text(
              bean.adName,
              maxLines: 2,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 16.0, color: Colors.black),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 5.0),
          child: CachedNetworkImage(
            imageUrl: bean.adImage == null
                ? 'images/img_loadfailure.png'
                : bean.adImage,
            errorWidget: (context, url, error) =>
                Image.asset('images/img_loadfailure.png', fit: BoxFit.fill),
            width: ScreenUtils.getInstance().getWidth(375.0) - 30,
            height: ScreenUtils.getInstance().getWidth(375.0) / 3 * 2,
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 0, 5.0, 5.0),
          child: Container(
            width: ScreenUtils.getInstance().getWidth(375.0) - 30,
            height: 15.0,
            child: source,
          ),
        )
      ],
    );

    return Card(
      elevation: 4.0,
      child: InkWell(
        child: column,
        onTap: () {
          _itemClick(widget.itemData);
        },
      ),
    );
  }

  void _itemClick(HomeContentBean itemData) async {
    print("_itemClick module" + itemData.module);
    print("_itemClick moduleId" + itemData.moduleId);
    switch (itemData.module) {
      case "article":
        await Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) {
          return new WebPage(
            title: "梅斯医学",
            url: "https://news.medsci.cn/article/show_article.do?id=" +
                itemData.moduleId,
            parentContext: context,
          );
        }));
        break;
      case "guider":
        await Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) {
          return new WebPage(
            title: "指南详情",
            url: "https://news.medsci.cn/guideline/show_article.do?id=" +
                itemData.moduleId,
            parentContext: context,
          );
        }));
        break;
      case "tool_impact_factor":
        tool_impact_factor bean = tool_impact_factor.fromJson(itemData.attach);
        await Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) {
          return new WebPage(
            title: bean.abbr,
            url: "https://news.medsci.cn/sci/journal-discuss?id=" +
                itemData.moduleId,
            parentContext: context,
          );
        }));
        break;
      case "advertisement":
        advertisement bean = advertisement.fromJson(itemData.attach);
        await Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) {
          return new WebPage(
            title: bean.adName,
            url: bean.adUrl,
            parentContext: context,
          );
        }));
        break;
    }
  }
}
