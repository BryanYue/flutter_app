import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/api/api.dart';
import 'package:flutterapp/api/http_util.dart';
import 'package:flutterapp/api/util/DataUtils.dart';
import 'package:flutterapp/api/util/constants.dart';
import 'package:flutterapp/api/util/login_event.dart';
import 'package:flutterapp/api/util/screen_utils.dart';
import 'package:flutterapp/data/UserInfo.dart';
import 'package:flutterapp/ui/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new MePageState();
  }
}

class MePageState extends State<MePage> {
  bool flag = false;
  String userID;
  String token;
  ScrollController _contraller = ScrollController();
  List<Meicon> list = new List();
  UserInfo userInfo;

  @override
  void initState() {
    super.initState();
    _getUser();
    initList();
    Constants.eventBus.on<LoginEvent>().listen((event) {
      print("eventBus");
      initList();
      _getUser();
    });
  }

  @override
  void dispose() {
    _contraller.dispose();
    super.dispose();
  }

  void _getUser() async {
    DataUtils.getUserName().then((userId) {
      setState(() {
        this.userID = userId;
        print('userId:' + userId.toString());
        getUserInfo();
      });
    });
    DataUtils.getToken().then((token) {
      setState(() {
        this.token = token;
        print('token:' + token.toString());
      });
    });
    DataUtils.isLogin().then((isLogin) {
      setState(() {
        this.flag = isLogin;
        print('isLogin:' + isLogin.toString());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget listView = ListView.builder(
      itemCount: list.length + 1,
      itemBuilder: (context, i) => buildItem(i),
      controller: _contraller,
    );

    return RefreshIndicator(child: listView, onRefresh: _pullToRefresh);
  }

  Future<Null> _pullToRefresh() async {
    initList();
    _getUser();
    return null;
  }

  void getUserInfo() {
    Map<String, String> map = new Map();
    map['userId'] = userID;
    DioManager().request<UserInfo>(NWMethod.POST, Api.GET_USER_INFO,
        params: map, success: (data) {
      if (data != null) {
        setState(() {
          userInfo = data;
        });
      }
    }, error: (error) {
      print("error code = ${error.status}, massage = ${error.message}");
    });
  }

  void initList() {
    list.clear();
    list.add(new Meicon("同好圈", "images/list_icn_fs.png"));
    list.add(new Meicon("积分商城", "images/list_icn_jfsc.png"));
    list.add(new Meicon("赚积分", "images/list_icn_jf.png"));
    list.add(new Meicon("Mini梅斯医生", "images/list_icn_msys.png"));
    list.add(new Meicon("向小M反馈", "images/i_xm.png"));
    list.add(new Meicon("病例讨论", "images/i_bltl.png"));
    list.add(new Meicon("我的话题", "images/topic.png"));
    list.add(new Meicon("我的收藏", "images/list_icn_sc.png"));
    list.add(new Meicon("我的上传", "images/sc_d.png"));
    list.add(new Meicon("指南下载记录", "images/list_icn_zn.png"));
    list.add(new Meicon("设置", "images/list_icn_sz.png"));
    if (flag) {
      list.add(new Meicon("退出登录 ", "images/list_icn_sz.png"));
    }
  }

  Widget buildItem(int i) {
    if (i == 0) {
      return Container(
        height: 190.0,
        child: buildHead(),
      );
    } else {
      Meicon itemData = list[i - 1];

      ListTile title = ListTile(
        leading: Container(
          margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          width: ScreenUtils.getInstance().getWidth(375.0) / 16 - 2,
          height: ScreenUtils.getInstance().getWidth(375.0) / 16 - 2,
          child: Image.asset(itemData.icon, fit: BoxFit.fill),
        ),
        title: Text(itemData.title),
        trailing: Icon(Icons.arrow_forward_ios),
        onTap: () {
          _itemClick(i - 1, itemData);
        },
      );

      return Card(
        elevation: 4.0,
        child: InkWell(
          child: title,
          onTap: () {
            _itemClick(i - 1, itemData);
          },
        ),
      );
    }
  }

  Widget buildHead() {
    Row name;
    print("buildHead flag =${flag}, userInfo = ${userInfo}");
    if (flag && userInfo != null) {
      name = Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        Container(
            margin: EdgeInsets.fromLTRB(0.0, 12.0, 0.0, 5.0),
            width: ScreenUtils.getInstance().getWidth(375.0) / 6,
            height: ScreenUtils.getInstance().getWidth(375.0) / 7,
            child: CachedNetworkImage(
              imageUrl: userInfo.userCommonResponse.avatar == null
                  ? 'images/user_pp.png'
                  : userInfo.userCommonResponse.avatar,
              errorWidget: (context, url, error) =>
                  Image.asset('images/user_pp.png', fit: BoxFit.fill),
              width: ScreenUtils.getInstance().getWidth(375.0) / 6,
              height: ScreenUtils.getInstance().getWidth(375.0) / 7,
            )),
        Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
            child: Text(
              userInfo.userCommonResponse.userName == null
                  ? ""
                  : userInfo.userCommonResponse.userName,
              style: TextStyle(fontSize: 15.0, color: Colors.black),
              maxLines: 1,
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 12.0, 0.0, 5.0),
                  width: ScreenUtils.getInstance().getWidth(375.0) / 6,
                  height: 20,
                  child: Text(
                    userInfo.userCommonResponse.integralBase == null
                        ? "0积分"
                        : userInfo.userCommonResponse.integralBase.toString() +
                            "积分",
                    style: TextStyle(fontSize: 15.0, color: Colors.black),
                    maxLines: 1,
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 12.0, 0.0, 5.0),
                  width: ScreenUtils.getInstance().getWidth(375.0) / 6,
                  height: 20,
                  child: Text(
                    userInfo.userCommonResponse.integralUme == null
                        ? "0梅花"
                        : userInfo.userCommonResponse.integralUme.toString() +
                            "0梅花",
                    style: TextStyle(fontSize: 15.0, color: Colors.black),
                    maxLines: 1,
                  ),
                )
              ],
            ),
          ),
        ])
      ]);
    } else {
      name = Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 12.0, 0.0, 12.0),
          width: ScreenUtils.getInstance().getWidth(375.0) / 6,
          height: ScreenUtils.getInstance().getWidth(375.0) / 6,
          child: Image.asset('images/user_pp.png', fit: BoxFit.fill),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(12.0, 18.0, 0.0, 18.0),
          width: ScreenUtils.getInstance().getWidth(375.0) / 2,
          height: 18.0,
          child: Text(
            "您还没有登录",
            style: TextStyle(fontSize: 15.0, color: Colors.black),
            maxLines: 1,
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 18.0, 0.0, 18.0),
          width: ScreenUtils.getInstance().getWidth(375.0) / 4,
          height: 30.0,
          child: OutlineButton(
            onPressed: () {
              _toLoinClick();
            },
            child: Text("马上登录"),
            textColor: Colors.blueAccent,
            borderSide: BorderSide(color: Colors.blueAccent, width: 1),
          ),
        )
      ]);
    }

    Column attentionNum = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: Text(
            userInfo == null
                ? "0"
                : userInfo.getCountInfoResponse.attentionNum.toString(),
            maxLines: 1,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16.0, color: Colors.black),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
          child: Text(
            "关注",
            maxLines: 1,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16.0, color: Colors.grey),
          ),
        ),
      ],
    );

    Column fansNum = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: Text(
            userInfo == null
                ? "0"
                : userInfo.getCountInfoResponse.fansNum.toString(),
            maxLines: 1,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16.0, color: Colors.black),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
          child: Text(
            "粉丝",
            maxLines: 1,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16.0, color: Colors.grey),
          ),
        ),
      ],
    );

    Column messageNum = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: Text(
            userInfo == null
                ? "0"
                : userInfo.getCountInfoResponse.messageNum.toString(),
            maxLines: 1,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16.0, color: Colors.black),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
          child: Text(
            "私信",
            maxLines: 1,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16.0, color: Colors.grey),
          ),
        ),
      ],
    );

    Column edit = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
          child: Container(
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            width: ScreenUtils.getInstance().getWidth(375.0) / 16 - 2,
            height: ScreenUtils.getInstance().getWidth(375.0) / 16 - 2,
            child: Image.asset('images/i_bjzl.png', fit: BoxFit.fill),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
          child: Text(
            "编辑资料",
            maxLines: 1,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16.0, color: Colors.grey),
          ),
        ),
      ],
    );

    Row bottom = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            final snackBar = SnackBar(content: Text("点击了 attentionNum"));
            Scaffold.of(context).showSnackBar(snackBar);
          },
          child: Container(
            margin: EdgeInsets.fromLTRB(0.0, 0, 0.0, 0),
            width: ScreenUtils.getInstance().getWidth(375.0) / 5,
            height: ScreenUtils.getInstance().getWidth(375.0) / 6,
            child: attentionNum,
          ),
        ),
        VerticalDivider(
          color: Colors.grey,
          thickness: 1,
        ),
        GestureDetector(
          onTap: () {
            final snackBar = SnackBar(content: Text("点击了 fansNum"));
            Scaffold.of(context).showSnackBar(snackBar);
          },
          child: Container(
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            width: ScreenUtils.getInstance().getWidth(375.0) / 5,
            height: ScreenUtils.getInstance().getWidth(375.0) / 6,
            child: fansNum,
          ),
        ),
        VerticalDivider(
          color: Colors.grey,
          thickness: 1,
        ),
        GestureDetector(
          onTap: () {
            final snackBar = SnackBar(content: Text("点击了 messageNum"));
            Scaffold.of(context).showSnackBar(snackBar);
          },
          child: Container(
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            width: ScreenUtils.getInstance().getWidth(375.0) / 5,
            height: ScreenUtils.getInstance().getWidth(375.0) / 6,
            child: messageNum,
          ),
        ),
        VerticalDivider(
          color: Colors.grey,
          thickness: 1,
        ),
        GestureDetector(
          onTap: () {
            final snackBar = SnackBar(content: Text("点击了 edit"));
            Scaffold.of(context).showSnackBar(snackBar);
          },
          child: Container(
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            width: ScreenUtils.getInstance().getWidth(375.0) / 5,
            height: ScreenUtils.getInstance().getWidth(375.0) / 6,
            child: edit,
          ),
        ),
      ],
    );

    Column column = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          child: name,
        ),
        Divider(
          color: Colors.grey,
          thickness: 1,
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          child: IntrinsicHeight(
            child: bottom,
          ),
        ),
      ],
    );
    return Card(
      elevation: 4.0,
      child: InkWell(
        child: column,
      ),
    );
  }

  void _itemClick(int postion, Meicon item) {
    print("_itemClick postion" + postion.toString());
    final snackBar = SnackBar(content: Text("点击了 " + item.title));
    Scaffold.of(context).showSnackBar(snackBar);
    if (postion == 11) {
      DataUtils.clearLoginInfo().then((value) {
        flag = false;
        userInfo = null;
        userID = null;
        token = null;
        initList();
        _getUser();
      });
    }
  }

  void _toLoinClick() async {
    await Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return new LoginPage();
    }));
  }
}

class Meicon {
  String title;
  String icon;

  Meicon(this.title, this.icon);
}
