import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/api/api.dart';
import 'package:flutterapp/api/http_util.dart';
import 'package:flutterapp/api/util/screen_utils.dart';
import 'package:flutterapp/data/DomainInfo.dart';
import 'package:flutterapp/data/NewNewsInfo.dart';
import 'package:flutterapp/ui/web_page.dart';

class ArticleListPage extends StatefulWidget {
  final DomainInfo info;

  ArticleListPage(this.info);

  @override
  State<StatefulWidget> createState() {
    return new ArticleListState();
  }
}

class ArticleListState extends State<ArticleListPage>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => true;
  int curPage = 0;
  int listTotalSize = 500;
  String classid = "";
  List<NewNewsInfo> list = new List();
  List<TransverseCategory> items = new List();
  List<Tab> tabs = new List();
  ScrollController _controller = ScrollController();
  TabController _tabContro;

  @override
  Widget build(BuildContext context) {
    Widget page;
    if (items.length > 0) {
      page = DefaultTabController(
        length: items.length,
        child: Scaffold(
          appBar: _buildTabBar(),
          body: list == null
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.builder(
                  itemCount: list.length,
                  itemBuilder: (context, i) => buildItem(i),
                  controller: _controller,
                ),
        ),
      );
    } else {
      page = list == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: list.length,
              itemBuilder: (context, i) => buildItem(i),
              controller: _controller,
            );
    }

    return RefreshIndicator(child: page, onRefresh: _pullToRefresh);
  }

  @override
  void initState() {
    super.initState();

    if (widget.info.transverseCategory != null) {
      items.addAll(widget.info.transverseCategory);
      for (TransverseCategory value in items) {
        tabs.add(Tab(text: value.titleCn));
      }
      _tabContro = TabController(length: items.length, vsync: this);
    }

    _getArticleList();
    _controller.addListener(() {
      var maxScroll = _controller.position.maxScrollExtent;
      var pixels = _controller.position.pixels;
      if (maxScroll == pixels && list.length < listTotalSize) {
        _getArticleList();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _tabContro.dispose();
    _controller.dispose();
  }

  void _getArticleList() {
    Map<String, String> map = new Map();
    if (widget.info.titleCn == "推荐") {
      map['pageIndex'] = curPage.toString();
      map['pageSize'] = "10";
      map['typeId'] = "2";
      DioManager().requestList<NewNewsInfo>(NWMethod.POST, Api.RECOMMEND_INFO,
          params: map, success: (data) {
        if (data != null) {
          setState(() {
            if (curPage == 0) {
              list.clear();
            }
            curPage++;
            list.addAll(data);
          });
        }
      }, error: (error) {
        print("error code = ${error.status}, massage = ${error.message}");
      });
    } else {
      Map<String, String> map = new Map();
      map['pageIndex'] = curPage.toString();
      map['pageSize'] = "10";
      map['categoryId'] = widget.info.id.toString();
      if (classid != "") {
        map['subCategoryId'] = classid;
      }

      DioManager().requestList<NewNewsInfo>(NWMethod.POST, Api.CATEGORY_INFO,
          params: map, success: (data) {
        if (data != null) {
          setState(() {
            if (curPage == 0) {
              list.clear();
            }
            curPage++;
            list.addAll(data);
          });
        }
      }, error: (error) {
        print("error code = ${error.status}, massage = ${error.message}");
      });
    }
  }

  Future<Null> _pullToRefresh() async {
    curPage = 0;
    _getArticleList();
    return null;
  }

  Widget _buildTabBar() => TabBar(
        onTap: (tab) {
          print(tab);
          classid = items[tab].categoryId.toString();
          curPage = 0;
          _getArticleList();
        },
        labelStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        unselectedLabelStyle: TextStyle(fontSize: 16),
        isScrollable: true,
        controller: _tabContro,
        labelColor: Colors.blue,
        unselectedLabelColor: Colors.grey,
        tabs: tabs,
      );

  Widget buildItem(int i) {
    if (i < list.length && i >= 0) {
      var itemData = list[i];
      if (itemData != null) {
        return buildArticle(itemData);
      }
    }
  }

  Widget buildArticle(NewNewsInfo item) {
    Row title = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1.5,
          height: ScreenUtils.getInstance().getWidth(375.0) / 5 * 1,
          child: CachedNetworkImage(
            imageUrl:
                item.cover == null ? 'images/img_loadfailure.png' : item.cover,
            errorWidget: (context, url, error) =>
                Image.asset('images/img_loadfailure.png', fit: BoxFit.fill),
          ),
        ),
        Container(
          width: ScreenUtils.getInstance().getWidth(375.0) / 5 * 3,
          height: 90.0,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                child: Text(
                  item.chTitle,
                  style: TextStyle(fontSize: 16.0, color: Colors.grey),
                  maxLines: 2,
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
                child: Text(
                  item.publishedTimeString,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 12.0, color: Colors.grey),
                  maxLines: 1,
                ),
              ),
            ],
          ),
        ),
      ],
    );

    return Card(
      elevation: 4.0,
      child: InkWell(
        child: title,
        onTap: () {
          _itemClick(item);
        },
      ),
    );
  }

  void _itemClick(NewNewsInfo itemData) async {
    print("_itemClick module" + itemData.module);
    print("_itemClick moduleId" + itemData.moduleId);

    await Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return new WebPage(
        title: "梅斯医学",
        url: "https://news.medsci.cn/article/show_article.do?id=" +
            itemData.moduleId,
        parentContext: context,
      );
    }));
  }
}
