import 'package:flutter/material.dart';
import 'package:flutterapp/data/DomainInfo.dart';

import 'VideoListPage.dart';

class VideoPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new VideoPageState();
  }
}

class VideoPageState extends State<VideoPage>
    with SingleTickerProviderStateMixin {
  List<ChannelItem> list = new List();

  List<Tab> tabs = new List();

  TabController _tabContro;

  @override
  void initState() {
    super.initState();

    list.clear();
    tabs.clear();
//    list.add(new ChannelItem(0, "推荐"));
    list.add(new ChannelItem(2, "心血管"));
    list.add(new ChannelItem(4, "消化"));
    list.add(new ChannelItem(5, "肿瘤科"));
    list.add(new ChannelItem(6, "内分泌科"));
    list.add(new ChannelItem(9, "感染"));
    list.add(new ChannelItem(17, "神经科"));

    for (ChannelItem value in list) {
      tabs.add(Tab(text: value.name));
      print(value.name);
    }
    _tabContro = TabController(length: list.length, vsync: this);
  }

  @override
  void dispose() {
    _tabContro.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (tabs.length > 0) {
      return DefaultTabController(
        length: list.length,
        child: Scaffold(
          appBar: _buildTabBar(),
          body: _buildTableBarView(),
        ),
      );
    } else {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  Widget _buildTabBar() => TabBar(
        onTap: (tab) => print(tab),
        labelStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        unselectedLabelStyle: TextStyle(fontSize: 16),
        isScrollable: true,
        controller: _tabContro,
        labelColor: Colors.blue,
        unselectedLabelColor: Colors.grey,
        tabs: tabs,
      );

  Widget _buildTableBarView() => TabBarView(
      controller: _tabContro,
      children: list.map((ChannelItem itemData) {
        return VideoListPage(itemData.list_id.toString());
      }).toList());
}
