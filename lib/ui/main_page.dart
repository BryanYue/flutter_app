import 'package:flutter/material.dart';
import 'package:flutterapp/constants/colors.dart';
import 'package:flutterapp/ui/home_page.dart';
import 'package:flutterapp/ui/news_page.dart';
import 'package:flutterapp/ui/video_page.dart';
import 'package:flutterapp/ui/me_page.dart';

//主页
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with TickerProviderStateMixin {
  int _tabIndex = 0;

  List<BottomNavigationBarItem> _navigationViews;

  var appBarTitles = ['首页', '资讯', '视频', '我的'];

  var _body;

  initData() {
    _body = IndexedStack(
      children: <Widget>[HomePage(), NewsPage(), VideoPage(), MePage()],
      index: _tabIndex,
    );
  }

  @override
  void initState() {
    super.initState();

    _navigationViews = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: const Icon(Icons.home),
        title: Text(appBarTitles[0]),
        backgroundColor: Colors.blue,
      ),
      BottomNavigationBarItem(
        icon: const Icon(Icons.featured_play_list),
        title: Text(appBarTitles[1]),
        backgroundColor: Colors.blue,
      ),
      BottomNavigationBarItem(
        icon: const Icon(Icons.video_library),
        title: Text(appBarTitles[2]),
        backgroundColor: Colors.blue,
      ),
      BottomNavigationBarItem(
        icon: const Icon(Icons.person),
        title: Text(appBarTitles[3]),
        backgroundColor: Colors.blue,
      ),
    ];
  }

  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    initData();
    return MaterialApp(
      navigatorKey: navigatorKey,
      theme: ThemeData(
          primaryColor: AppColors.colorPrimary, accentColor: Colors.blue),
      home: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(40),
            child: Offstage(
              offstage: _tabIndex == 0 ? true : false,
              child: AppBar(
                centerTitle: true,
                title: Text(
                  appBarTitles[_tabIndex],
                  style: TextStyle(color: Colors.white),
                ),
                actions: <Widget>[
                  IconButton(
                      icon: Icon(Icons.search),
                      onPressed: () {
//                为什么不直接Navigator.push(context,
//                   MaterialPageRoute(
//                      builder: (context) =>  SearchPage()))
//                  https://stackoverflow.com/questions/50124355/flutter-navigator-not-working

//                  navigatorKey.currentState.push(MaterialPageRoute(builder: (context) {
//                    return SearchPage(null);
//                  }));
                      })
                ],
              ),
            ),
            ),
        body: _body,
        bottomNavigationBar: BottomNavigationBar(
          items: _navigationViews
              .map((BottomNavigationBarItem navigationView) => navigationView)
              .toList(),
          currentIndex: _tabIndex,
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              _tabIndex = index;
            });
          },
        ),
      ),
    );
  }
}
