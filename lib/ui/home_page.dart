import 'package:flutter/material.dart';
import 'package:flutterapp/api/api.dart';
import 'package:flutterapp/api/http_util.dart';
import 'package:flutterapp/data/HomeContentBean.dart';
import 'package:flutterapp/data/home_aboveBean.dart';
import 'package:flutterapp/ui/item.dart';
import 'package:flutterapp/widget/slide_view.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  List<HomeContentBean> list = new List();
  var curPage = 0;
  var listTotalSize = 500;
  home_aboveBean home;
  SlideView _bannerView;

  ScrollController _contraller = ScrollController();
  TextStyle titleTextStyle = TextStyle(fontSize: 15.0);
  TextStyle subtitleTextStyle = TextStyle(color: Colors.blue, fontSize: 12.0);

  HomePageState() {
    _contraller.addListener(() {
      var maxScroll = _contraller.position.maxScrollExtent;
      var pixels = _contraller.position.pixels;

      if (maxScroll == pixels && list.length < listTotalSize) {
        getHomeArticlelist();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (list == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      Widget listView = ListView.builder(
        itemCount: list.length + 1,
        itemBuilder: (context, i) => buildItem(i),
        controller: _contraller,
      );

      return RefreshIndicator(child: listView, onRefresh: _pullToRefresh);
    }
  }

  @override
  void initState() {
    super.initState();
    getBanner();
    getHomeArticlelist();
  }

  @override
  void dispose() {
    _contraller.dispose();
    super.dispose();
  }

  Future<Null> _pullToRefresh() async {
    curPage = 0;
    getBanner();
    getHomeArticlelist();
    return null;
  }

  void getBanner() {
    DioManager().request<home_aboveBean>(NWMethod.POST, Api.NEW_HOME_ABOVE_URL,
        params: null, success: (data) {
      home = data;

      _bannerView = SlideView(home.adList);
    }, error: (error) {
      print("error code = ${error.status}, massage = ${error.message}");
    });
  }

  void getHomeArticlelist() {
    Map<String, String> map = new Map();
    map['pageIndex'] = curPage.toString();
    map['pageSize'] = "10";
    DioManager().requestList<HomeContentBean>(
        NWMethod.POST, Api.NEW_HOME_LIST_DATA,
        params: map, success: (data) {
      if (data != null) {
        setState(() {
          if (curPage == 0) {
            list.clear();
          }
          curPage++;
          list.addAll(data);
        });
      }
    }, error: (error) {
      print("error code = ${error.status}, massage = ${error.message}");
    });
  }

  Widget buildItem(int i) {
    if (i == 0) {
      return Container(
        height: 180.0,
        child: _bannerView,
      );
    }
    if (i - 1 < list.length && i - 1 >= 0) {
      var itemData = list[i - 1];
      if (itemData != null) {
        return ArticleItem(itemData);
      }
    }
  }
}
