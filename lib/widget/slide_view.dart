import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/data/home_aboveBean.dart';
import 'package:flutterapp/ui/article_detail_page.dart';
import 'package:flutterapp/ui/article_detail_page.dart';
import 'package:flutterapp/ui/web_page.dart';

class SlideView extends StatefulWidget {
  List<AdListBean> _adlist = new List();

  SlideView(this._adlist);

  @override
  State<StatefulWidget> createState() {
    return new SlideViewState(_adlist);
  }
}

class SlideViewState extends State<SlideView>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  List<AdListBean> _adlist = new List();

  SlideViewState(this._adlist);

  @override
  void initState() {
    super.initState();
    tabController = new TabController(
        length: _adlist == null ? 0 : _adlist.length, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> items = [];
    if (_adlist != null && _adlist.length > 0) {
      for (var i = 0; i < _adlist.length; i++) {
        AdListBean item = _adlist[i];
        var imgUrl = item.adImage;
        var name  =item.adName;
        var link = item.adUrl;
        items.add(new GestureDetector(
            onTap: () {
              _handOnItemClick(name,link);
            },
            child: AspectRatio(
              aspectRatio: 2.0 / 1.0,
              child: new Stack(
                children: <Widget>[
                  new CachedNetworkImage(
                    imageUrl: imgUrl,
                    fit: BoxFit.cover,
                    width: 1000.0,
                  ),
                  new Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: new Container(
                      width: 1000.0,
                      color: const Color(0x50000000),
                      padding: const EdgeInsets.all(5.0),
                      child: new Text(item.adName,
                          style: new TextStyle(
                              color: Colors.white, fontSize: 15.0)),
                    ),
                  ),
                ],
              ),
            )));
      }
    }
    return new TabBarView(
      controller: tabController,
      children: items,
    );
  }

  void _handOnItemClick(String name,String url) async {
    await Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return new WebPage(title: name, url: url);
    }));
  }
}
